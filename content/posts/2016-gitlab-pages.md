Title: Pelican on GitLab Pages!
Date: 2016-03-25
Category: GitLab
Tags: pelican, gitlab
Slug: pelican-on-gitlab-pages

Rob's test Pelican Site

This site is hosted on GitLab Pages!

The source code of this site is at <https://gitlab.com/ardencaple/pelican>.

Learn about GitLab Pages at <https://pages.gitlab.io>.
